import { BaseEntity } from 'typeorm';
export declare class Message extends BaseEntity {
    constructor(sender: string, message: string);
    id: string;
    sender: string;
    message: string;
    date: Date;
}
