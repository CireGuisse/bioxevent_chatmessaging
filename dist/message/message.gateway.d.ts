import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { MessageService } from './message.service';
export declare class MessageGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {
    messageService: MessageService;
    wss: Server;
    private logger;
    private count;
    handleDisconnect(client: any): Promise<void>;
    handleConnection(client: any, ...args: any[]): Promise<void>;
    afterInit(server: any): Promise<void>;
    handleNewMessage(client: Socket, data: {
        sender: string;
        message: string;
    }): Promise<void>;
}
