"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a, _b;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const common_1 = require("@nestjs/common");
const socket_io_1 = require("socket.io");
const message_service_1 = require("./message.service");
let MessageGateway = class MessageGateway {
    constructor() {
        this.logger = new common_1.Logger('MessageGateway');
        this.count = 0;
    }
    async handleDisconnect(client) {
        this.count -= 1;
        this.logger.log(`Disconnected: ${this.count} connections`);
    }
    async handleConnection(client, ...args) {
        this.count += 1;
        this.logger.log(`Connected: ${this.count} connections`);
        const messages = await this.messageService.getAll();
        client.emit('all-messages-to-client', messages);
    }
    async afterInit(server) {
        this.logger.log('MessageGateway Initialized');
    }
    async handleNewMessage(client, data) {
        const message = await this.messageService.createMessage(data.sender, data.message);
        this.wss.emit('new-message-to-client', { message });
    }
};
__decorate([
    common_1.Inject(),
    __metadata("design:type", message_service_1.MessageService)
], MessageGateway.prototype, "messageService", void 0);
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", typeof (_a = typeof socket_io_1.Server !== "undefined" && socket_io_1.Server) === "function" ? _a : Object)
], MessageGateway.prototype, "wss", void 0);
__decorate([
    websockets_1.SubscribeMessage('new-message-to-server'),
    __param(0, websockets_1.ConnectedSocket()), __param(1, websockets_1.MessageBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [typeof (_b = typeof socket_io_1.Socket !== "undefined" && socket_io_1.Socket) === "function" ? _b : Object, Object]),
    __metadata("design:returntype", Promise)
], MessageGateway.prototype, "handleNewMessage", null);
MessageGateway = __decorate([
    websockets_1.WebSocketGateway(4000, { namespace: 'message' })
], MessageGateway);
exports.MessageGateway = MessageGateway;
//# sourceMappingURL=message.gateway.js.map