import { Message } from './message.entity';
export declare class MessageService {
    getAll: () => Promise<Message[]>;
    createMessage: (sender: string, message: string) => Promise<Message>;
}
