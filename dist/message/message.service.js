"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageService = void 0;
const common_1 = require("@nestjs/common");
const message_entity_1 = require("./message.entity");
let MessageService = class MessageService {
    constructor() {
        this.getAll = async () => {
            return message_entity_1.Message.find();
        };
        this.createMessage = async (sender, message) => {
            const newMessage = new message_entity_1.Message(sender, message);
            return await newMessage.save();
        };
    }
};
MessageService = __decorate([
    common_1.Injectable()
], MessageService);
exports.MessageService = MessageService;
//# sourceMappingURL=message.service.js.map